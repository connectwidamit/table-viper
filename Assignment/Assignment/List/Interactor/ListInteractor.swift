
class ListInteractor: ListInteractorInputProtocol {
    weak var presenter: ListInteractorOutputProtocol?
    var localDatamanager: ListLocalDataManagerInputProtocol?
    var remoteDatamanager: ListLocalDataManagerInputProtocol?
    
    func retrievePostList(){
        do {
            if let postList = try localDatamanager?.retrieveList() {
                presenter?.didRetrievePosts(postList)
                
            } else {
               let retryList =  remoteDatamanager?.retrieveList()
                presenter?.didRetrievePosts(retryList!)
            }

        } catch {
            presenter?.didRetrievePosts([])
        }
    }

}

extension ListInteractor: ListLocalDataManagerOutputProtocol {

    func onPostsRetrieved(_ posts: [ListModel]) {
        presenter?.didRetrievePosts(posts)

        
    }

    func onError() {
        presenter?.onError()
    }

}
