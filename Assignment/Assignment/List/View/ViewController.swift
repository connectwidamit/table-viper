//
//  ViewController.swift
//  Assignment
//
//  Created by Amit on 21/06/19.
//  Copyright © 2019 Amit. All rights reserved.
//

import UIKit
import LUExpandableTableView
class ViewController: UIViewController {
    
    private let expandableTableView = LUExpandableTableView()
    
    private let cellReuseIdentifier = "MyCell"
    private let sectionHeaderReuseIdentifier = "MySectionHeader"
    
    var presenter: ListPresenterProtocol?
    var postList: [ListModel] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(expandableTableView)
        
        
        
        expandableTableView.register(MyTableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        expandableTableView.register(UINib(nibName: "MyExpandableTableViewSectionHeader", bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: sectionHeaderReuseIdentifier)
        
        expandableTableView.expandableTableViewDataSource = self
        expandableTableView.expandableTableViewDelegate = self
        self.presenter?.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        expandableTableView.estimatedRowHeight = 60
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        expandableTableView.frame = view.bounds
        expandableTableView.frame.origin.y += 20
    }
    
}

extension ViewController: ListViewProtocol {
    
    func showPosts(with posts: [ListModel]) {
        postList = posts
        print(postList)
        expandableTableView.reloadData()
    }
    
    func showError() {
        print("Error occured ")
    }
    
    func showLoading() {
    }
    
    func hideLoading() {
    }
    
}

extension ViewController: LUExpandableTableViewDataSource {
    func numberOfSections(in expandableTableView: LUExpandableTableView) -> Int {
        return postList.count
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, numberOfRowsInSection section: Int) -> Int {
        print(postList[section].sub_category!.count)
        return postList[section].sub_category!.count
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = expandableTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as? MyTableViewCell else {
            assertionFailure("Cell shouldn't be nil")
            return UITableViewCell()
        }
        
        let dataObj = postList[indexPath.section].sub_category![indexPath.row]
        print("Amit--->",dataObj.name)
        
        cell.summary_label.tag = indexPath.row
        if let nameLabel = cell.viewWithTag(100) as? UILabel{
            print("ache")
            cell.label.text = dataObj.name
            nameLabel.text = dataObj.display_name
            expandableTableView.reloadRows(at: [indexPath], with: .none)
            
        }else{
            cell.label.text = dataObj.name
            cell.summary_label.text = dataObj.display_name
            cell.summary_label.tag = 100
           // cell.frame.height = 0.0
            cell.contentView.addSubview(cell.label)
            cell.contentView.insertSubview(cell.summary_label, belowSubview: cell.label)
            expandableTableView.reloadRows(at: [indexPath], with: .none)

        }
        
        
      
        return cell
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, sectionHeaderOfSection section: Int) -> LUExpandableTableViewSectionHeader {
        guard let sectionHeader = expandableTableView.dequeueReusableHeaderFooterView(withIdentifier: sectionHeaderReuseIdentifier) as? MyExpandableTableViewSectionHeader else {
            assertionFailure("Section header shouldn't be nil")
            return LUExpandableTableViewSectionHeader()
        }
        let dataObj = postList[section]
        
        sectionHeader.label.text = dataObj.name
        
       
        return sectionHeader
    }
}

// MARK: - LUExpandableTableViewDelegate

extension ViewController: LUExpandableTableViewDelegate {
    func expandableTableView(_ expandableTableView: LUExpandableTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        /// Returning `UITableViewAutomaticDimension` value on iOS 9 will cause reloading all cells due to an iOS 9 bug with automatic dimensions
       let sectionindex = indexPath.section
        let rowindex = indexPath.row
       
        let text = postList[sectionindex].sub_category![rowindex].display_name
       
        let height = heightForView(text: text!, width: expandableTableView.frame.width)
        
        print("amitiiiiiii",height)
        return  height + 20
        
    }
    func heightForView(text:String, width:CGFloat) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.thin)
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
    func expandableTableView(_ expandableTableView: LUExpandableTableView, heightForHeaderInSection section: Int) -> CGFloat {
        /// Returning `UITableViewAutomaticDimension` value on iOS 9 will cause reloading all cells due to an iOS 9 bug with automatic dimensions
        return 69
    }
    
    // MARK: - Optional
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, didSelectRowAt indexPath: IndexPath) {
        print(" select cell at section \(indexPath.section) row \(indexPath.row)")
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, didSelectSectionHeader sectionHeader: LUExpandableTableViewSectionHeader, atSection section: Int) {
        print(" select section header at section \(section)")
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        print(" Will display cell at section \(indexPath.section) row \(indexPath.row)")
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, willDisplaySectionHeader sectionHeader: LUExpandableTableViewSectionHeader, forSection section: Int) {
        print(" Will display section header for section \(section)")
        
        
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func expandableTableView(_ expandableTableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    
}
