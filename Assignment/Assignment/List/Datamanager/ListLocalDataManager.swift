//
//  ListLocalDataManager.swift
//  Assignment
//
//  Created by Amit on 21/06/19.
//  Copyright © 2019 Amit. All rights reserved.
//

import Foundation
class ListLocalDataManager:ListLocalDataManagerInputProtocol{
   
    

    var remoteRequestHandler: ListLocalDataManagerOutputProtocol?

    func retrieveList() -> [ListModel] {
        if let path = Bundle.main.path(forResource: "AllMenu", ofType: "json") {
            do {
                // 1
                let decoder = JSONDecoder()
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
               // let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
             
                let listobject = try decoder.decode([ListModel].self, from: data)
                    self.remoteRequestHandler?.onPostsRetrieved(listobject)
                return listobject
               
            } catch {
                self.remoteRequestHandler?.onError()
                // handle error
                return []
            }
        }else{
            return []
        }
   }
}
