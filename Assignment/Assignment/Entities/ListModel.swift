//
//  ListModel.swift
//  Assignment
//
//  Created by Amit on 21/06/19.
//  Copyright © 2019 Amit. All rights reserved.
//

import Foundation
import ObjectMapper




struct ListModel:Decodable {
    var name :String?
    var sub_category:[sub_category]?
    
}
struct sub_category:Decodable {
    var name:String?
    var display_name:String?
}


